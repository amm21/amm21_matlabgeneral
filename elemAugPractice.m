%{
Elementary Row Operations & Augmented Matrix
4/9/18

1. Swap two rows
2. Multiply a single row by a nonzero number
3. Add a scalar multiple of one row to another

1. (eq1 * -1) + eq2
2. (eq1 * -2) + eq3
3. swap eq2 & eq3
this leaves with upper triangle matrix
%}

clear;
clc;

A = [1 2 3; 1 2 1; 2 9 5];
B = [10; 4; 27];
AB = [A, B];

AB(2,:) = ( AB(1,:)*-1 + AB(2,:) );
AB(3,:) = ( AB(1,:)*-2 + AB(3,:) );

AB([3 2],:) = AB([2 3],:);

A = AB(:, 1:3);
B = AB(:, 4);
