%{

%}

function [C] = addTwo(A, B)
C = [];

for i = 1:3
    for j = 1:2
        C(i,j) = A(i,j) + B(i,j);
    end
end

end % end function