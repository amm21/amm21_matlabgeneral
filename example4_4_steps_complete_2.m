% Example 4-4 on pp. 117
function Xs = example4_4_steps_complete_2(A, B)

clc;
% the augmented matrix
% A = [4 5 -2; 2 -5 2; 6 2 4];
% B = [-6; 24; 30];
% Xs = example4_4_steps_complete_2(A, B)
[rNum, cNum] = size(A);
AB = [A, B]; 

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i =1:rNum
% the first pivoting row is the ith row
% the first element A(1,1) is the pivot element
% the row is normalized by dividing the pivot element
    AB(i,:) = AB(i,:)/AB(i,i);

    % Next, all the first elements in other rows are eliminated
    for j = 1:rNum
        if j ~= i
            AB(j,:) = AB(i,:)*(-AB(j,i)) + AB(j,:);
        end
    end
end
Xs = AB(:, cNum+1)

% done


