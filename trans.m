%{
%}

function [B] = trans(A)

    for i = 1:size(A,2)
        for j = 1:size(A,1)
            B(i,j) = A(j,i);
        end
    end

end % end function