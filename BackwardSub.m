%{
Input: A, B
Output: X
Procedure:
1. 
%}

function x = BackwardSub(a,b)

n = length(b);
x(n,1) = b(n)/a(n,n);

    for i = n-1 : -1 : 1
        
        x(i,1) = ( b(i) - a(i, i+1:n) * x(i+1:n, 1) ) ./ a(i,i);
        
    end % end for

end % end function Backward Sub