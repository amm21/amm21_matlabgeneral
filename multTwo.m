%{
Input: A array, s scalar
Output: B array 
Procedure:
    1. Initialize i = first value in size(A)
    2. Initialize j = second value in size(B)
    3. Initialize B to empty array
    4. Set B(i,j) to A(i,j)*s
    5. Increment i and j by - 1
    6. Repeat 3 and 4 until i and j both equal 0
%}

function [B] = multTwo(A, s)

for i = 1:size(A,1)
    for j = 1:size(A,2)
        B(i,j) = A(i,j)*s;
    end
end
end % end function

