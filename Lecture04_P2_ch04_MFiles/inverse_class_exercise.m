%
clc
%
A1 = [1 3 0 -1; 2 6 -4 4; 1 0 -1 -9; 0 1 0 3];
[nR, nC] = size(A1);
disp('The matrix is:');
disp(A1);

I = eye(4);

% the augmented matrix
A = [A1, I];
disp('The working matrix is:');
disp(A);

%%%%%%%%%%%%%%%%%%%% iPivotRow = 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the first pivoting row is the first row
% the first element here is the pivot element
% the row is normalized by dividing the pivot element
iPivotRow = 1;
A(iPivotRow, :) = A(iPivotRow, :)/A(iPivotRow, iPivotRow);

% Next, all the first elements in other rows are eliminated
iEliminateRow = 2;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 3;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 4;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));


%%%%%%%%%%%%%%% swap rows if necessary %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Swap the 2nd and 4th row
A([2 4],:) = A([4 2],:);


%%%%%%%%%%%%%%%%%% iPivotRow = 2 %%%%%%%%%%%%%%%%%%%%%%%%%
% the next pivot row is the second row
% the pivot element is A(2,2)
% the row is normalized by dividing the pivot element
iPivotRow = 2;
A(iPivotRow, :) = A(iPivotRow, :)/A(iPivotRow, iPivotRow);

% Next, all the first elements in other rows are eliminated
iEliminateRow = 1;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 3;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

% redundant but helpful for developing functions
iEliminateRow = 4;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));


%%%%%%%%%%%%%%%%%%% iPivotRow = 3 %%%%%%%%%%%%%%%%%%%%%%%
% the next pivot row is the third row
% the pivot element is A(3,3)
% the row is normalized by dividing the pivot element
iPivotRow = 3;
A(iPivotRow, :) = A(iPivotRow, :)/A(iPivotRow, iPivotRow);

% Next, all the first elements in other rows are eliminated
% redundant but helpful for developing functions
iEliminateRow = 1;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

% redundant but helpful for developing functions
iEliminateRow = 2;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 4;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));


%%%%%%%%%%%%%%%% iPivotRow = 4 %%%%%%%%%%%%%%%%%%%%%%%%%
% the next pivot row is the fourth row
% the pivot element is A(4,4)
% the row is normalized by dividing the pivot element
iPivotRow = 4;
A(iPivotRow, :) = A(iPivotRow, :)/A(iPivotRow, iPivotRow);

% Next, all the first elements in other rows are eliminated
iEliminateRow = 1;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 2;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

iEliminateRow = 3;
A(iEliminateRow, :) = A(iEliminateRow, :) + A(iPivotRow,:)*(-A(iEliminateRow, iPivotRow)/A(iPivotRow, iPivotRow));

disp('Result after Guass-Jordan elimination:');
disp(A);

% extract the last several columns to form the inverse
[nRA, nCA] = size(A);
invA = A(:, nC+1:nCA);

disp('The inverse is:');
disp(invA);




