% Example 4-4 on pp. 117

% the augmented matrix
AB = [4 -2 -3 6 12; -6 7 6.5 -6 -6.5; 1 7.5 6.25 5.5 16; -12 22 15.5 -1 17];

% the first pivoting row is the first row
% the first element A(1,1) is the pivot element
% the row is normalized by dividing the pivot element
AB(1,:) = AB(1,:)/AB(1,1);

% Next, all the first elements in other rows are eliminated
AB(2,:) = AB(1,:)*(-AB(2,1)) + AB(2,:);
AB(3,:) = AB(1,:)*(-AB(3,1)) + AB(3,:);
AB(4,:) = AB(1,:)*(-AB(4,1)) + AB(4,:);

% the next pivot row is the second row
% the pivot element is A(2,2)
% the row is normalized by dividing the pivot element
AB(2,:) = AB(2,:)/AB(2,2);

% Next, all the second elements in other rows are eliminated
AB(1,:) = AB(2,:)*(-AB(1,2)) + AB(1,:);
AB(3,:) = AB(2,:)*(-AB(3,2)) + AB(3,:);
AB(4,:) = AB(2,:)*(-AB(4,2)) + AB(4,:);

% the next pivot row is the 3rd row
% the pivot element is A(3,3)
% the row is normalized by dividing the pivot element


% Next, all the 3rd elements in other rows are eliminated


% the next pivot row is the 4th row
% the pivot element is A(4,4)
% the row is normalized by dividing the pivot element


% Next, all the 3rd elements in other rows are eliminated


% done


