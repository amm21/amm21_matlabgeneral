F = @(x)8-4.5*(x-sin(x));
a = 2;
b = 3;


if F(a)*F(b) < 0 % step 1
i = 1;
estErr = 1;
xns = ((a*F(b)) - (b*F(a)))/(F(b)-F(a)); % step 2
prevIt = 1;

    while estErr > 0.001 && i < 10 % step 4

        if (F(a)*F(xns)) < 0 % step 3
            b = xns;
        elseif (F(a)*F(xns)) > 0
            a = xns;
        end % end nested if/else    
               
        % estErr = (b-a)/2; % book strategy
        estErr = abs(F(xns)); % tolerance
        % estErr = abs((prevIt-xns)/prevIt); % E.R.E.
        fprintf('Error at iteration %d: %d\n', i, abs(estErr));
        
        % prevIt = xns; % E.R.E.
        xns = ((a*F(b)) - (b*F(a)))/(F(b)-F(a));  
        i = i + 1;
        
    end % end while
    
end % end if