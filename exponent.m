%{
Alexis Miller
CSCI B240 (SOFTWARE ENGINEERING)
This is a function created to test Homework 1's IOP.

Problem statement: Write a function to find the exponentiation of b^n, 
where the base is b and the exponent is n. b and n are variables. 
For example, if b=2 and n=5 the exponentiation is 2^5 and the function 
returns 32.

Input: two positive integers, b for base and n for exponent
Output: integer exp for exponentiation
Procedure/Actions: 
    Step 1. Initialize exp to be exp = 1;
    Step 2. Update exp = exp * b; (multiply the current value of exp by b)
    Step 3. Update n to n = n - 1; (reduce the "power" by 1)
    Step 4. Repeat steps 3-4 until n = 0 (stop when you have multiplied b
            by itself n times)
%}

function [exp] = exponent(b, n)

% Step 1. Initialize exp to be exp = 1;
exp = 1;
    
    % Step 4. Repeat steps 3-4 until n = 0
    while n > 0
        % Step 2. Update exp = exp * b;
        exp = exp * b;
        % Step 3. Update n to n = n - 1;
        n = n - 1;
    end % end while

end % end function