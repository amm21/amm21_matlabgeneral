%{
Alexis Miller
CSCI B280 Computational Mathematics

Input: dec (integer)
Output: binary (array)
Procedure:
    Step 1:  Initialize binary to an empty array
    Step 2:  Initalize rem to 0 (to represent remainder)
    Step 3:  Initialize sign to true (to assume positive number)
    Step 4:  If dec < 0, set sign to false (to show it's negative)
    Step 5:  If dec < 0, set dec = abs(dec);
    Step 6:  Update rem to be the value of dec%2 (remainder)
    Step 7:  If rem above 0, add a "1" to front of binary array and 
             set dec = dec/2 - rem/2
    Step 8:  If rem is 0, add a "0" to front of binary array and
             set dec = dec/2
    Step 9:  Repeat steps 3-6 until dec = 0
    Step 10: If sign == false (from the positive/negative "check" 
             at the start), add a "-" to binary array (represent negative)
%}

function [binary] = convertNum(dec)

% Step 1: Initialize binary to an empty array
binary = [];
% Step 2: Initalize rem to 0 (to represent remainder)
rem = 0;
sign = true;

    if dec < 0
        % Step 3: If dec < 0, add '-' to binary array.
        sign = false;
        % Step 4: If dec < 0, set dec = abs(dec);
        dec = abs(dec);
    end % end if
   
    
    % Step 9: Repeat steps 3-6 until dec = 0
    while dec > 0
        % Step 5: Update rem to be the value of dec%2 
        rem = mod(dec, 2);
        % Step 6: Update dec to be the value of dec/2 using fix
        % dec = fix(dec/2);
        
        % Step 7: If rem is above 0, add a "1" to front of binary array
        if rem > 0
            binary = ['1', binary];
            dec = dec/2 - rem/2; % test line to replace step 6 
        % Step 8: If rem 0, add a "0" to front of binary array    
        elseif rem == 0
            binary = ['0', binary];
            dec = dec/2; % test line to replace step 6
        end % end if
           
    end % end while  
    
    if sign == false
        binary = ['-', binary];
    end % end if
    
end % end function convertNum

