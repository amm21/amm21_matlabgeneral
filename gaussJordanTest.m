%{

%}
clear;
clc;
A = [4 5 -2; 2 -5 2; 6 2 4];
B = [-6; 24; 30];
AB = [A, B];

AB(1,:) = AB(1,:)/AB(1,1); % make the first pivot coeff 1

% eliminate first element in all other rows
AB(2,:) = AB(1,:)*(-AB(2,1)) + AB(2,:);
AB(3,:) = AB(1,:)*(-AB(3,1)) + AB(3,:);

AB(2,:) = AB(2,:)/AB(2,2); % make your next pivot coeff 1

% eliminate second element in all other rows
AB(1,:) = AB(2,:)*(-AB(1,2)) + AB(1,:);
AB(3,:) = AB(2,:)*(-AB(3,2)) + AB(3,:);

AB(3, :) = AB(3,:)/AB(3,3); % make next pivot coeff 1

% eliminate third element in all other rows
AB(1,:) = AB(3,:)*(-AB(1,3)) + AB(1,:);
AB(2,:) = AB(3,:)*(-AB(2,3)) + AB(2,:);

% extract the solutions ("B" row) from AB and name it X
X = AB(:,4)