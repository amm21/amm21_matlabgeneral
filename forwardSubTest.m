clear;
clc;

A = [-2 0 0; -1 5 0; 3 2 1];
B = [-6; 7; 10];
X = [];
n = 3;
i = n-1;

X(1) = B(1)/A(1,1);
%{
X(2,1) = ( B(2) - ( A(2,1)*X(1,1) ) ) ./ A(2,2);

X(3,1) = ( B(3) - ( A(3,1)*X(1,1) + A(3,2)*X(2,1) ) ) ./ A(3,3);
%}

for i = 2:3
    total = 0;
    for j = 1:i-1
        total = A(i,j)*X(j) + total;
    end % end inner for
    X(i) = ( B(i) - total) / A(i,i);
end % end for