function C = matrixMult(A,B)
C = [];

for i = 1:size(A,2)
    for j = 1:size(B,1) 
        for k = 1:2
            C(i,j) = A(i,k) * B(k,j);
    end
end

end