% Script to simulate the paper work for Example 4-1 on pp.106
% Xuwei Liang @ USCB
clear;
clc;

% Coefficient matrix A
A=[4 -2 -3 6; -6 7 6.5 -6; 1 7.5 6.25 5.5; -12 22 15.5 -1];

% Vector B
B=[12 -6.5 16 17];
B=B';

% Augmented matrix AB
AB=[A, B];

% Apply Guass elimination method
% Step 1: The 1st row is the pivot row
AB(2, :) = AB(2, :) + 1.5*AB(1, :);
AB(3, :) = AB(3, :) - 0.25*AB(1, :);
AB(4, :) = AB(4, :) + 3*AB(1, :);

% Step 2: The second row is the pivot row
AB(3, :) = AB(3, :) - 2*AB(2, :);
AB(4, :) = AB(4, :) - 4*AB(2, :);

% Step 3: the 3rd row is now the pivot row
AB(4, :) = AB(4, :) + 0.5*AB(3, :);

%
B1 = AB(:, 5);
A1 = AB(:, [1:4]);
X = A1\B1
