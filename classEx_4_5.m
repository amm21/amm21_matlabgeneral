% Class exercise for LU Crout's decomposition
clear
clc

A = [4 -2 -3 6; -6 7 6.5 -6; 1 7.5 6.25 5.5; -12 22 15.5 -1];
B=[12 -6.5 16 17];
[R, C] = size(A);
% Create L and U
L=zeros(R);
U=zeros(R);

% Step 1
L(1:R, 1) = A(1:R, 1);

% Step 2
for i=1:R
    U(i,i) = 1;
end

% step 3
U(1, 2:R) = A(1, 2:R)/L(1,1);

%step 4
for i = 2:R
   for j = 2:i
       L(i,j) = A(i,j) - L(i, 1:j-1)*U(1:j-1, j);
   end
   for j = i+1:R
      U(i,j) = (A(i,j) - L(i, 1:i-1)*U(1:i-1, j))/L(i,i) 
   end
end
A
B
L;
U;

Y = ForwardSub(L, B);
X = BackwardSub(U, Y)
