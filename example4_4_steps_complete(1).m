% Example 4-4 on pp. 117

% the augmented matrix
AB = [4 -2 -3 6 12; -6 7 6.5 -6 -6.5; 1 7.5 6.25 5.5 16; -12 22 15.5 -1 17];
% % A script to test function "example4_4.m"
% % A = [4 -2 -3 6; -6 7 6.5 -6; 1 7.5 6.25 5.5; -12 22 15.5 -1];
% % B = [12; -6.5; 16; 17 ];
% % X = example4_4(A, B)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%i=1;
% the first pivoting row is the first row
% the first element here is the pivot element
% the row is normalized by dividing the pivot element
%AB(i, :) = AB(i, :)/AB(i,i);
AB(1,:) = AB(1,:)/AB(1,1);

% Next, all the first elements in other rows are eliminated
%for j=1:r
%    if j ~= i
%        AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
%    end
%end
%for j=2:4
 %       AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
%end
%j=2;
%AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
AB(2,:) = AB(1,:)*(-AB(2,1)) + AB(2,:);
%j=3;
%AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
AB(3,:) = AB(1,:)*(-AB(3,1)) + AB(3,:);
%j=4
%AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
AB(4,:) = AB(1,:)*(-AB(4,1)) + AB(4,:);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%i=2;
% the next pivot row is the second row
% the pivot element is AB(2,2)
% the row is normalized by dividing the pivot element
%AB(i, :) = AB(i, :)/AB(i,i);
AB(2,:) = AB(2,:)/AB(2,2);

% Next, all the second elements in other rows are eliminated

%for j=1:r
%    if j ~= i
%        AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
%    end
%end
AB(1,:)=AB(1,:)-AB(2,:)*AB(1,2);
AB(3,:)=AB(3,:)-AB(2,:)*AB(3,2);
AB(4,:)=AB(4,:)-AB(2,:)*AB(4,2);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%i=3;
% the next pivot row is the 3rd row
% the pivot element is AB(3,3)
% the row is normalized by dividing the pivot element
%AB(i, :) = AB(i, :)/AB(i,i);
AB(3,:) = AB(3,:)/AB(3,3);

% Next, all the 3rd elements in other rows are eliminated
%for j=1:r
%    if j ~= i
%        AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
%    end
%end
AB(1,:) = AB(1,:) - AB(3,:)*AB(1,3);
AB(2,:) = AB(2,:) - AB(3,:)*AB(2,3);
AB(4,:) = AB(4,:) - AB(3,:)*AB(4,3);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%i=4;
% the next pivot row is the 4th row
% the pivot element is AB(4,4)
% the row is normalized by dividing the pivot element
%AB(i, :) = AB(i, :)/AB(i,i);
AB(4,:) = AB(4,:)/AB(4,4);

% Next, all the 3rd elements in other rows are eliminated
%for j=1:r
%    if j ~= i
%        AB(j, :) = AB(j, :) - AB(i,:)*AB(j,i);
%    end
%end
AB(1,:) = AB(1,:) - AB(4,:)*AB(1,4);
AB(2,:) = AB(2,:) - AB(4,:)*AB(2,4);
AB(3,:) = AB(3,:) - AB(4,:)*AB(3,4);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% done
% Output the solutions
% AB;
% A = AB(:, 1:(c-1));
% X = AB(:, c);