%{
Newton Method practice
p70, ex3-3
%}
clear
clc

%F = @(x)(1/x)-2;
%F1 = @(x)(-1/(x.^2));
F = @(x)8-4.5*(x-sin(x));
F1 = @(x)-4.5*(1-cos(x));

% x = 1.4, then 1, then 0.4
x1 = 2;
fprintf('The current value is: %d\n', x1);
x2 = x1 - (F(x1)/F1(x1));
% OR this x2 = 2*(x1-(x1^2));
fprintf('The current value is: %d\n', x2);
x3 = x2 - (F(x2)/F1(x2));
fprintf('The current value is: %d\n', x3);
x4 = x3 - (F(x3)/F1(x3));
fprintf('The current value is: %d\n', x4);

