%{
Bisection method example
F = @(x)8-4.5*(x-sin(x));
[a,b] = [2,3]
a = 2;
b = 3;

%}

y = 0;
x = 0:0.1:4;
intv = [0, 4];
F = @(x)8-4.5*(x-sin(x));
fplot(F, intv);
hold on;
plot(x, y)
