F = @(x)8-4.5*(x-sin(x));
a = 2;
b = 3;

tester = F(a) * F(b);

if tester < 0 
i = 1;
estErr = 1;
x1 = (a + b)/2;
prevIt = 1;

    while estErr > 0.001 && i < 10

        if F(a)*F(x1) < 0
            b = x1;
        elseif F(a)*F(x1) > 0
            a = x1;
        end % end nested if/else    
        
        
        % estErr = (b-a)/2; % book strategy
        % estErr = abs(F(x1)); % tolerance
        estErr = abs((prevIt-x1)/prevIt); % E.R.E.
        fprintf('Error at iteration %d: %d\n', i, abs(estErr));
        
        prevIt = x1; % E.R.E.
        x1 = (a + b)/2;
        
        i = i + 1;
        
    end % end while
    
end % end if