%{
4x1 - 2x2 - 3x3 + 6x4 = 12
-6x1 + 7x2 + 6.5x3 - 6x4 = -6.5
x1 + 7.5x2 + 6.25x3 + 5.5x4 = 16
-12x1 + 22x2 + 15.5x3 - x4 = 17

loop
i = 2 to # of eq
j = i - 1
AB(i,:) = AB(i,:) + ( AB(j,:) * -( AB(i,j) / AB(j,j) );
%}

clear;
clc;

A = [4 -2 -3 6; -6 7 6.5 -6; 1 7.5 6.25 5.5; -12 22 15.5 -1];
B = [12; -6.5; 16; 17];
AB = [A, B];
[n, C] = size(A);

% pivot needed
%A = [1 3 0 -1; 2 6 -4 4; 1 0 -1 -9; 0 1 0 3];
%B = [-8; 4; -35; 10];

% step 1 (eliminate x1)
for i = 2:n
    for j = 1
        AB(i,:) = AB(i,:) + ( AB(j,:) * -( AB(i,j) / AB(j,j) ) );
    end
end
%AB(2,:) = AB(2,:) + (AB(1,:) * -( AB(2,1) / AB(1,1) ));
%AB(3,:) = AB(3,:) + (AB(1,:) * -( AB(3,1) / AB(1,1) ));
%AB(4,:) = AB(4,:) + (AB(1,:) * -( AB(4,1) / AB(1,1) ));

% step 2 (eliminate x2)
for i = 3:n
    for j = 2
        AB(i,:) = AB(i,:) + ( AB(j,:) * -( AB(i,j) / AB(j,j) ) );
    end
end
%AB(3,:) = AB(3,:) + (AB(2,:) * -( AB(3,2) / AB(2,2) ));
%AB(4,:) = AB(4,:) + (AB(2,:) * -( AB(4,2) / AB(2,2) ));

% step 3 (eliminate x3)
for i = 4:n
    for j = 3
        AB(i,:) = AB(i,:) + ( AB(j,:) * -( AB(i,j) / AB(j,j) ) );
    end
end
%AB(4,:) = AB(4,:) + (AB(3,:) * -( AB(4,3) / AB(3,3) ));