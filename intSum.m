% Alexis Miller
% CSCI B280 Computational Mathematics
% Input: n
% Output: sum (an integer)
% Procedure:
%   Step 1: Initialize sum = 0 and i = 1
%   Step 2: Add current sum to the next integer until you reach n

% function [output] = name(input)
function [sum] = intSum(n) 

% Initialize sum to 0 and i to 1
sum = 0;
i = 1;

% Add the current sum to the next unprocessed integer until you have 
% added the value n.
while i <= n
sum = sum + i; 
i = i + 1;
end
