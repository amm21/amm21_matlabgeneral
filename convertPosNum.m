%{
Alexis Miller
CSCI B280 Computational Mathematics
Input: dec (integer)
Output: binary (array)
Procedure:
    Step 1: Initialize binary to an empty array
    Step 2: Initalize rem to 0 (to represent remainder)
    Step 3: Update rem to be the value of dec%2 
    Step 4: Update dec to be the value of dec/2 using fix  
    Step 5: If rem above 0, add a "1" to binary array
    Step 6: If rem is 0, add a "0" to binary array
    Step 7: Repeat steps 3-6 until dec = 0
    Step 8: Flip the direction of binary array using fliplr
%}

function [binary] = convertPosNum(dec)

% Step 1: Initialize binary to an empty array
binary = [];
% Step 2: Initalize rem to 0 (to represent remainder)
rem = 0;

    % Step 7: Repeat steps 3-6 until dec = 0
    while dec > 0
        % Step 3: Update rem to be the value of dec%2 
        rem = mod(dec, 2);
        % Step 4: Update dec to be the value of dec/2 using fix
        dec = fix(dec/2);
        
        % Step 5: If rem is above 0, add a "1" to binary array
        if rem > 0
            binary = [binary, '1'];
        % Step 6: If rem 0, add a "0" to binary array    
        elseif rem == 0
            binary = [binary, '0'];
        end % end if
           
    end % end while
    
    % Step 8: Flip the direction of binary array using fliplr
    binary = fliplr(binary);

end % end function convertNum

